.TH bzadmin 6
.SH NAME
bzadmin \- a text based client for BZFlag
.SH SYNOPSIS
.sS
.B bzadmin
[\fB\-help\fR]
[\fB\-ui \fR{\fIcurses\fR | \fIstdboth\fR | \fIstdin\fR | \fIstdout\fR}]
\fIcallsign@hostname\fR:[\fIport\fR] [\fIcommand\fR] [\fIcommand\fR] ...
.SH DESCRIPTION
.B bzadmin
is a textbased client for the game BZFlag. It can't be used for
playing, but it can be used to see when players join and leave the
game, to see the chat messages, and to send messages and commands
to the server.
.PP
When you start bzadmin without any command line options other than
callsign and hostname a simple curses-based user interface will be
started (unless you built bzadmin without curses support). This
interface is divided into three rectangles; the output window
(which covers almost all of the terminal), the target window, and
the input window.
.PP
The output window is where messages from the server will be shown.
.PP
The target window shows the name of the player that
will receive your next message, or 'all' if it will be a public message.
You can change target by using the left and right arrow keys.
.PP
The input window is where you type your messages. It also supports tab
completion of commands and callsigns. You can clear the input window
with the key combination \fBCtrl-U\fR, and you can generate a \fB/kick\fR
command for the current target with the F5 key (if the current target
is a player).
.SS Options
.RS
.TP 15
.B -help
Show a simple help text.
.TP
\fB\-ui \fR{\fIcurses\fR | \fIstdboth\fR | \fIstdin\fR | \fIstdout\fR}
Select the user interface that you want. The curses interface is the default,
and it is described above.
.br
The stdin interface reads user commands from the standard in stream
and sends them to the server. All server output is ignored.
.br
The stdout interface prints all server output to the standard out stream.
All user input is ignored.
.br
The stdboth interface is a combination of stdin and stdout - it prints
server output to the standard out stream, and reads user commands from
the standard in stream.
.TP
\fIcallsign@hostname\fR[\fI:port\fR]
Specifies the callsign that you want your client to use, and the
hostname where the BZFlag server is. The port number is optional,
and the default value is 5154.
.TP
\fIcommand\fR
You can specify messages and commands to send to the server on the
command line. If any such commands are specified, bzadmin will
exit as soon as those commands has been sent, without starting
an user interface.
.RE
.SS Examples
.RS
.TP 15
\fBbzadmin admin@localhost:5154\fR
.br
Join the game on localhost, port 5154, using the callsign 'admin'.
.TP
\fBbzadmin admin@localhost '/passwd secretpass' '/ban 192.168.0.2'
.br
Connect to the server at localhost and ban the IP 192.168.0.2.
.TP
\fBbzadmin -ui stdout spy@bzserver.xy | grep magicword\fR
Connect to bzserver.xy and print all server messages that contain 'magicword'.
.RE
.SH SEE ALSO
bzfs(6), bzflag(6)
